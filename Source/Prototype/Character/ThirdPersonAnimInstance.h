// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "ThirdPersonAnimInstance.generated.h"

/**
 * 
 */

UCLASS()
class PROTOTYPE_API UThirdPersonAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
public:

	UFUNCTION(BlueprintCallable, Category = SetState)
		void SetMovementSpeed(float input);
	UFUNCTION(BlueprintCallable, Category = SetState)
		void SetDirection(float input);
	UFUNCTION(BlueprintCallable, Category = SetState)
		void SetAimingDegree(float input);
	UFUNCTION(BlueprintCallable, Category = SetState)
		void SetEnableJump(bool input);
	UFUNCTION(BlueprintCallable, Category = SetState)
		void SetJumping(bool input);
	UFUNCTION(BlueprintCallable, Category = SetState)
		void SetCrouching(bool input);
	UFUNCTION(BlueprintCallable, Category = SetState)
		void SetAiming(bool input);
	UFUNCTION(BlueprintCallable, Category = SetState)
		void SetIsDead(bool input);
	UFUNCTION(BlueprintCallable, Category = PlayAnimation)
		void OneShotAnimation();

	UFUNCTION(BlueprintCallable, Category = GetState)
		FORCEINLINE float GetMovementSpeed() const { return MovementSpeed; }
	UFUNCTION(BlueprintCallable, Category = GetState)
		FORCEINLINE float GetDirection() const { return Direction; }
	UFUNCTION(BlueprintCallable, Category = GetState)
		FORCEINLINE float GetAimingDegree() const { return Direction; }
	UFUNCTION(BlueprintCallable, Category = GetState)
		FORCEINLINE bool GetEnableJump() const { return EnableJump; }
	UFUNCTION(BlueprintCallable, Category = GetState)
		FORCEINLINE bool GetJumping() const { return Jumping; }
	UFUNCTION(BlueprintCallable, Category = GetState)
		FORCEINLINE bool GetCrouching() const { return Crouching; }
	UFUNCTION(BlueprintCallable, Category = GetState)
		FORCEINLINE bool GetAiming() const { return Aiming; }
	UFUNCTION(BlueprintCallable, Category = GetState)
		FORCEINLINE bool GetIsDead() const { return IsDead; }
protected:
	UFUNCTION(BlueprintCallable, Category = UpdateAnimation)
		void UpdateAnimationProperties();
	UFUNCTION(BlueprintCallable, Category = UpdateAnimation)
		void StartJumping();
	UFUNCTION(BlueprintCallable, Category = UpdateAnimation)
		void FinishJumping();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = AnimationState)
		float MovementSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = AnimationState)
		float Direction;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = AnimationState)
		float AimingDegree;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = AnimationState)
		bool EnableJump;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = AnimationState)
		bool Jumping;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = AnimationState)
		bool Crouching;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = AnimationState)
		bool Aiming;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = AnimationState)
		bool IsDead;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = AnimationState)
		TArray<UAnimMontage*> MontageList;

	class AThirdPersonCharacter* OwnerCharacter;
};
