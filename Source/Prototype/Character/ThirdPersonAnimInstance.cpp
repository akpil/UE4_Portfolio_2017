// Fill out your copyright notice in the Description page of Project Settings.

#include "Character/ThirdPersonAnimInstance.h"
#include "Character/ThirdPersonCharacter.h"


void UThirdPersonAnimInstance::UpdateAnimationProperties()
{
	if (OwnerCharacter == nullptr)
	{
		OwnerCharacter = Cast<AThirdPersonCharacter>(TryGetPawnOwner());
	}
	else
	{
		FVector velocity = OwnerCharacter->GetVelocity();
		MovementSpeed = velocity.Size();
		Direction = CalculateDirection(velocity, OwnerCharacter->GetActorRotation());
		
		AimingDegree = OwnerCharacter->GetControlRotation().Pitch;
		if (AimingDegree > 180.f)
		{
			AimingDegree -= 360.f;
		}
	}
}

void UThirdPersonAnimInstance::StartJumping()
{
	if (OwnerCharacter == nullptr)
	{
		OwnerCharacter = Cast<AThirdPersonCharacter>(TryGetPawnOwner());
	}
	else
	{
		if (!Jumping)
		{
			OwnerCharacter->Jump();
			Jumping = true;
			EnableJump = false;
		}
	}
}

void UThirdPersonAnimInstance::FinishJumping()
{
	Jumping = false;
	EnableJump = false;
}

void UThirdPersonAnimInstance::SetMovementSpeed(float input)
{
	MovementSpeed = input;
}

void UThirdPersonAnimInstance::SetDirection(float input)
{
	Direction = input;
}

void UThirdPersonAnimInstance::SetAimingDegree(float input)
{
	AimingDegree = input;
}

void UThirdPersonAnimInstance::SetEnableJump(bool input)
{
	EnableJump = input;
}

void UThirdPersonAnimInstance::SetJumping(bool input)
{
	Jumping = input;
}

void UThirdPersonAnimInstance::SetCrouching(bool input)
{
	Crouching = input;
}

void UThirdPersonAnimInstance::SetAiming(bool input)
{
	Aiming = input;
}

void UThirdPersonAnimInstance::SetIsDead(bool input)
{
	IsDead = input;
}

void UThirdPersonAnimInstance::OneShotAnimation()
{
	if (!(MontageList.IsValidIndex(0)&&MontageList.IsValidIndex(1)))
	{
		UE_LOG(LogTemp, Warning, TEXT("Animation missing."));

		return;
	}
	if (Aiming)
	{
		Montage_Play(MontageList[1]);
	}
	else
	{
		Montage_Play(MontageList[2]);
	}
}
