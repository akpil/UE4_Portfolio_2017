// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ThirdPersonCharacter.generated.h"

UCLASS()
class PROTOTYPE_API AThirdPersonCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FollowCamera;

	/* AnimaInstance for controll animation */
	UPROPERTY(VisibleAnywhere, Category = Animation)
		class UThirdPersonAnimInstance* Anim;

	UPROPERTY(VisibleAnywhere, Category = SubObject)
		class AGun* GunActor;

public:
	AThirdPersonCharacter();

	void BeginPlay() override;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;	

	virtual void Jump() override;

	void OnDeath();

	virtual float TakeDamage(
		float DamageAmount,
		struct FDamageEvent const & DamageEvent,
		class AController * EventInstigator,
		AActor * DamageCauser
	) override;

	UFUNCTION(BlueprintCallable, Category = GetState)
		FORCEINLINE float GetHP() const { return CurrentHealth; }
	UFUNCTION(BlueprintCallable, Category = GetState)
		FORCEINLINE float GetAmmo() const { return AmmoLeft; }
	UFUNCTION(BlueprintCallable, Category = GetState)
		FORCEINLINE UThirdPersonAnimInstance* GetAnim() const { return Anim; }
protected:

	
	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	bool ClampSpeed(float *Value);

	void Jumping();
	virtual void StopJumping() override;

	void SetCrouch();
	void CancleCrouch();
	void SetAiming();
	void CancleAiming();

	void FireWeapon();

	/**
	* Called via input to turn at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void TurnAtRate(float Rate);

	/**
	* Called via input to turn look up/down at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	UPROPERTY(EditDefaultsOnly, Category = Setup)
		TSubclassOf<class AGun> GunBlueprint;
	UPROPERTY(EditDefaultsOnly, Category = Movement)
		float MaxSpeed;
	UPROPERTY(EditDefaultsOnly, Category = Movement)
		float AimingMaxSpeed;
	UPROPERTY(EditDefaultsOnly, Category = Movement)
		float CrouchingMaxSpeed;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = SetUp)
		int32 BaseHealth = 100;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Setup)
		int32 CurrentHealth; // Init at beginplay
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = SetUp)
		int32 AmmoLeft = 100;
protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override
	{
		// Set up gameplay key bindings
		check(PlayerInputComponent);
		PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AThirdPersonCharacter::Jumping);
		PlayerInputComponent->BindAction("Jump", IE_Released, this, &AThirdPersonCharacter::StopJumping);

		PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &AThirdPersonCharacter::SetCrouch);
		PlayerInputComponent->BindAction("Crouch", IE_Released, this, &AThirdPersonCharacter::CancleCrouch);

		PlayerInputComponent->BindAction("Aiming", IE_Pressed, this, &AThirdPersonCharacter::SetAiming);
		PlayerInputComponent->BindAction("Aiming", IE_Released, this, &AThirdPersonCharacter::CancleAiming);

		PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AThirdPersonCharacter::FireWeapon);

		PlayerInputComponent->BindAxis("MoveForward", this, &AThirdPersonCharacter::MoveForward);
		PlayerInputComponent->BindAxis("MoveRight", this, &AThirdPersonCharacter::MoveRight);

		// We have 2 versions of the rotation bindings to handle different kinds of devices differently
		// "turn" handles devices that provide an absolute delta, such as a mouse.
		// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
		PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
		PlayerInputComponent->BindAxis("TurnRate", this, &AThirdPersonCharacter::TurnAtRate);
		PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
		PlayerInputComponent->BindAxis("LookUpRate", this, &AThirdPersonCharacter::LookUpAtRate);

		// handle touch devices
		PlayerInputComponent->BindTouch(IE_Pressed, this, &AThirdPersonCharacter::TouchStarted);
		PlayerInputComponent->BindTouch(IE_Released, this, &AThirdPersonCharacter::TouchStopped);

		// VR headset functionality
		PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AThirdPersonCharacter::OnResetVR);
	}
	// End of APawn interface

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
};
