// Fill out your copyright notice in the Description page of Project Settings.

#include "ThirdPersonCharacter.h"


#include "Character/ThirdPersonCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Character/ThirdPersonAnimInstance.h"
#include "Weapon/Gun.h"

//////////////////////////////////////////////////////////////////////////
// APrototypeCharacter

AThirdPersonCharacter::AThirdPersonCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 500.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

												// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

												   // Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
												   // are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)	
	MaxSpeed = 270.f;
	AimingMaxSpeed = 150.f;
	CrouchingMaxSpeed = 100.f;
}

void AThirdPersonCharacter::BeginPlay()
{
	Super::BeginPlay();

	Anim = Cast<UThirdPersonAnimInstance>(GetMesh()->GetAnimInstance());
	CurrentHealth = BaseHealth;

	if (GunBlueprint == NULL) {

		UE_LOG(LogTemp, Warning, TEXT("Gun blueprint missing."));

		return;

	}

	GunActor = GetWorld()->SpawnActor<AGun>(GunBlueprint);
	GunActor->AttachToComponent(GetMesh(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));
}

//////////////////////////////////////////////////////////////////////////
// Input

void AThirdPersonCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AThirdPersonCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	Jump();
}

void AThirdPersonCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	StopJumping();
}

void AThirdPersonCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AThirdPersonCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AThirdPersonCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		if (!ClampSpeed(&Value))
		{
			UE_LOG(LogTemp, Warning, TEXT("Anim ptr is missing!"));
		}
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AThirdPersonCharacter::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		if (!ClampSpeed(&Value))
		{
			UE_LOG(LogTemp, Warning, TEXT("Anim ptr is missing!"));
		}
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

bool AThirdPersonCharacter::ClampSpeed(float * Value)
{
	if(ensure(Anim))
	{
		float throughput = 1.f;
		if (Anim->GetCrouching())
		{
			throughput = CrouchingMaxSpeed / MaxSpeed;
		}
		else
		{
			if (Anim->GetAiming())
			{
				throughput = AimingMaxSpeed / MaxSpeed;
			}
		}

		*Value = FMath::Clamp(*Value, -throughput, throughput);

		return true;
	}
	else
	{
		return false;
	}
}

void AThirdPersonCharacter::Jumping()
{
	check(Anim);
	if (!Anim->GetJumping())
	{
		Anim->SetEnableJump(true);
	}
}

void AThirdPersonCharacter::Jump()
{
	UE_LOG(LogTemp, Warning, TEXT("Jump Called"));
	ACharacter::Jump();
}

void AThirdPersonCharacter::OnDeath()
{
	Anim->SetIsDead(true);
}

float AThirdPersonCharacter::TakeDamage(float DamageAmount, FDamageEvent const & DamageEvent, AController * EventInstigator, AActor * DamageCauser)
{
	int32 DamagePoint = FPlatformMath::RoundToInt(DamageAmount);
	int32 DamageToApply = FMath::Clamp(DamagePoint, 0, CurrentHealth);

	CurrentHealth -= DamageToApply;
	if (CurrentHealth <= 0)
	{
		OnDeath();
	}
	UE_LOG(LogTemp, Warning, TEXT("%d"), CurrentHealth)
	return DamageToApply;
}

void AThirdPersonCharacter::StopJumping()
{
	ACharacter::StopJumping();
	check(Anim);
	Anim->SetEnableJump(false);
}

void AThirdPersonCharacter::SetCrouch()
{
	check(Anim);
	Anim->SetCrouching(true);
}

void AThirdPersonCharacter::CancleCrouch()
{
	check(Anim);
	Anim->SetCrouching(false);
}

void AThirdPersonCharacter::SetAiming()
{
	check(Anim);
	Anim->SetAiming(true);
}

void AThirdPersonCharacter::CancleAiming()
{
	check(Anim);
	Anim->SetAiming(false);
}

void AThirdPersonCharacter::FireWeapon()
{
	if (AmmoLeft-- > 0)
	{
		Anim->OneShotAnimation();
		GunActor->OnFire();
	}
	AmmoLeft = FMath::Clamp(AmmoLeft, 0, 999);
}


