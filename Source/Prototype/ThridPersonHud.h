// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "ThridPersonHud.generated.h"

/**
 * 
 */
UCLASS()
class PROTOTYPE_API AThridPersonHud : public AHUD
{
	GENERATED_BODY()
	
	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/** Crosshair asset pointer */
	UPROPERTY(EditDefaultsOnly, Category = setup)
		class UTexture2D* CrosshairTexture;

	UPROPERTY(EditDefaultsOnly, Category = setup)
		FVector2D CrosshairPosition;
	UPROPERTY(VisibleAnywhere, Category = setup)
		FVector2D Center;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = setup)
		class UThirdPersonAnimInstance* PlayerAnim;
};
