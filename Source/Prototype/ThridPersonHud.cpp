// Fill out your copyright notice in the Description page of Project Settings.

#include "ThridPersonHud.h"
#include "Character/ThirdPersonAnimInstance.h"
#include "Character/ThirdPersonCharacter.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Engine/Canvas.h"
#include "CanvasItem.h"


void AThridPersonHud::DrawHUD()
{
	if (PlayerAnim->GetAiming())
	{
		// draw the crosshair
		FCanvasTileItem TileItem(CrosshairPosition, CrosshairTexture->Resource, FLinearColor::White);
		TileItem.BlendMode = SE_BLEND_Translucent;
		Canvas->DrawItem(TileItem);
	}
}

void AThridPersonHud::BeginPlay()
{
	Super::BeginPlay();

	UE_LOG(LogTemp, Warning, TEXT("HUD"));
	AThirdPersonCharacter* OwnerCharacter = Cast<AThirdPersonCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (ensure(OwnerCharacter))
	{
		PlayerAnim = OwnerCharacter->GetAnim();
	}
}
