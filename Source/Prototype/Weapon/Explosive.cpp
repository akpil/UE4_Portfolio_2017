// Fill out your copyright notice in the Description page of Project Settings.

#include "Explosive.h"
#include "Runtime/Engine/Classes/Components/SphereComponent.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

// Sets default values
AExplosive::AExplosive()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));	

	ExplosiveBody = CreateDefaultSubobject<UStaticMeshComponent>(FName("ExplosiveMesh"));
	ExplosiveBody->SetupAttachment(CollisionComp);
	
	ExplosionBlast = CreateDefaultSubobject<UParticleSystemComponent>(FName("ExplosionLocation"));
	ExplosionBlast->bAutoActivate = false;
	ExplosionBlast->SetupAttachment(ExplosiveBody);
}

// Called when the game starts or when spawned
void AExplosive::BeginPlay()
{
	Super::BeginPlay();
	
}

void AExplosive::OnTimerExprier()
{
	Destroy();
}

// Called every frame
void AExplosive::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

float AExplosive::TakeDamage(float DamageAmount, FDamageEvent const & DamageEvent, AController * EventInstigator, AActor * DamageCauser)
{
	int32 DamagePoint = FPlatformMath::RoundToInt(DamageAmount);
	int32 DamageToApply = FMath::Clamp(DamagePoint, 0, CurrentHealth);
	CurrentHealth -= DamageToApply;
	if (CurrentHealth <= 0)
	{
		ExplosionBlast->Activate();
		if (ExplosionSound != nullptr)
		{
			UGameplayStatics::PlaySoundAtLocation(this, ExplosionSound, GetActorLocation());
		}
		UGameplayStatics::ApplyRadialDamage(GetWorld(), ExplosionDamage, GetActorLocation(), ExplosionRadius, UDamageType::StaticClass(), TArray<AActor*>(), this, nullptr, true);
		FTimerHandle Timer;
		GetWorld()->GetTimerManager().SetTimer(Timer, this, &AExplosive::OnTimerExprier, DestroyDelay, false);
	}
	UE_LOG(LogTemp, Warning, TEXT("Explosive Current Health : %d"), CurrentHealth);
	return DamageToApply;
}