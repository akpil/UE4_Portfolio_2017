// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Explosive.generated.h"


UCLASS()
class PROTOTYPE_API AExplosive : public AActor
{
	GENERATED_BODY()
	
	
public:	
	// Sets default values for this actor's properties
	AExplosive();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
		class USphereComponent* CollisionComp;

	UPROPERTY(EditDefaultsOnly, Category = Mesh)
		UStaticMeshComponent* ExplosiveBody;

	UPROPERTY(VisibleAnywhere, Category = Component)
		class URadialForceComponent* ExplosionForce;

	UPROPERTY(VisibleAnywhere, Category = Effect)
		class UParticleSystemComponent* ExplosionBlast;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Effect)
		class USoundBase* ExplosionSound;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = SetUp)
		int32 BaseHealth = 10;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Setup)
		int32 CurrentHealth;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = SetUp)
		int32 ExplosionDamage = 10;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = SetUp)
		float ExplosionRadius = 100.f;

	void OnTimerExprier();

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
		float DestroyDelay = 10.f;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual float TakeDamage(
		float DamageAmount,
		struct FDamageEvent const & DamageEvent,
		class AController * EventInstigator,
		AActor * DamageCauser
	) override;
};
