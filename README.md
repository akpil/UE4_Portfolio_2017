Simple UE4 shooting game

Binding animation (with animation starter pack clips)

Shoot object

explosion effect and affect to other object (shoot small UE sample statue then kill near character)

UI - Ammo & HP left, Crosshair appear when aim but it don't fit with real shoot target point

Play keys (with game pad)

------

WASD & Left jog = move

Mouse move & Right jog = view Direction

Left mouse Button & Right trigger = Shoot the gun

Right mouse Button & Left trigger = Aiming

Left shift & Left shoulder button = Crouching


-------